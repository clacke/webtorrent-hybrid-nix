#! /usr/bin/env nix-shell
#! nix-shell -i bash -p bash nodePackages.node2nix

cd ${BASH_SOURCE[0]%/*}

node2nix -8 --input <(echo '[ "webtorrent-hybrid" ]')
